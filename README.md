# Documentation for Cloyne's 3-D Printer

About: This page is an information repository for files, links, and history about the PrintrBot Simple Metal 3-D printer living in Cloyne's Makerspace. **When Printrbot went out of business,  
they pulled their entire knowledge base, wiki, documentation, and forums.** This page is one of the only reliable sources of information you will find on this 3-D printer.  
Everything written here is the result of my own search through data archives, and also my own personal experience. I've spent many, many hours go through calibrated searches of directories with file names such as
**eLe6h2jDk5NHuTpi.pdf**. 

Printer Technical Information
-------------------------------
Make: Printrbot (now defunct)  
Model: Simple Metal  
Bed size (mm): 150mm x 150mm x 150mm  
Bed size (in): 6" x 6" x 6"  
Filament: 1.75mm PLA  
Extruder: 0.4mm nozzle  
Power Requirements: At least 72w --> 12VDC/6A  

**Control**  
This printer will not print anything at all unless it can receive constant serial communication over bus from a laptop or other computer. This means that, unless you have a computer connected to it, it's a paperweight. The SD-print mechanism is/was broken. What I mean by this is that the only way to print a file from an SD card is to do a very specific process, wipe the entire SD card, and transfer the
carefully curated file to the SD card with the name *auto1.g*.  

Then you have to turn off the printer, reset it, insert the SD card while it's in the off-state, and then turn it on again. Even more confusing, is the fact that all G-Code files have the abbreviated extension *.g*. So, really, you would end up
naming the file *auto1.g.g*, which is a mess not just for stylistic reasons, but because file systems have no idea what to do with this kind of file, and it didn't even work most of the time.  

This *fun* array of issues led me to implement an OctoPrint server using an RPi 3B+ SBC, that allows automated control, remote monitoring, and more! I'm currently attaching a 2-DOF servo camera module for remote monitoring and general peeking around. 2-DOF means its powered by motors such that it can move along the X-axis and Y-axis of space, essentially up-down, right-left, or any combination of the two.



Buying filament
-----------------------
i. Make sure that it's 1.75mm PLA  
ii. Ensure spool inner hub diameter is >= 6.5cm (!)  
iii. More expensive filament != better.  
--> The quality of filament can be roughly measured by how strict the manufacturing tolerances are, not the quality of the PLA used.


Getting Started
-------------------
i. Go to https://thingiverse.com and find something you like  
ii. Click the "download" button on the page for whatever cool thing you clicked  
iii. Unzip the file to wherever is convenient  
iv. Go into the "wherever is convenient" location you unzipped the file in, and import the .stl file to your slicing platform of choice. //FIXME  
v. Slice the G-Code using whatever settings you need, and then upload it to the OctoPrint server (insert IP address here) //FIXME  
vi. The OctoPrint server will have the following authentication details:  
**user:** alice  
**password:** alicization


**Anecotes Log**  
**1:** Even within the data archives I could find, there existed **nothing** w.r.t. the spool rack. The spool rack is what holds the print filament so that it descends gracefully into the extruder mechanism.
This is an issue because, as it turns out, "normal"-sized 3-D print spools are incompatible with the aluminum spool rack design. And so, I had to use calipers to go ahead and measure the fact that, unless a spool has an inner hub diameter of 6.5cm or above, it won't work. 
  
**2:** Due to extruder mechanism issues, I had to take apart the extrusion mechanism. The feeding mechanism on this printer is very, very picky. If the tension is off, it will either refuse to feed or stop the print completely.
I ended up finding someone who made a locking extruder mechanism and posted the .stl files online, which I used to 3-D print one for the printer. This is good because it automatically locks the tension once filament is placed,
in such a way that feeding issues don't arise.